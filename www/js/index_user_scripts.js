/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
    
    
     /* button  Button */
    $(document).on("click", ".itemRadio.online", function(evt)
    {
        /*global activate_page */
        app.isPlaying = false;

        var srcStream = $(this).attr("data-srcStream");

        var idSenial = $(this).attr("data-senial");

        if(idSenial != 0) {
            $("#reproductor-senial").html(idSenial);
        }else{
            $("#reproductor-senial").html("");
        }

        var titulo = $(this).find("h2").html();

        $("#tituloTransmision").html(titulo);

        app.crearMenu(idSenial);

        app.mostrarPublicidades(idSenial); // Muestra las publicidades relacionadas a la senail seleccionada

        app.goToRadio(srcStream); //index.js

        activate_page("#reproductor");
        
        return false;
    });
    
    /* button  .uib_w_7 */
    $(document).on("click", ".btnPlayPause", function(evt)
    {
        /* your code goes here */ 
        app.changeToPlayPause()
        return false;
    });
     
    /* button  Button */
    $(document).on("click", ".btnBackMain", function(evt)
    {
        /*global activate_page */
        app.my_media.stop(); //Evita que el audio se cambie y reproduzca otro en 2do plano, perdiendo el puntero al audio que se está reproduciendo
        app.my_media = null; //Pierdo el puntero del audio Media
        activate_page("#mainPage");
        
        return false;
    });
     
    /* button  Button */
    $(document).on("click", ".btnExit", function(evt)
    {
        /*global activate_page */
        app.my_media.stop(); //Evita que el audio se cambie y reproduzca otro en 2do plano, perdiendo el puntero al audio que se está reproduciendo
        app.my_media = null; //Pierdo el puntero del audio Media
        navigator.app.exitApp();
        return false;
    });
     
    
    /* button  FACEBOOK  */
    $(document).on("click", "#btnFacebook", function(evt)
    {
        /*global activate_page */
        window.open("https://www.facebook.com/ekartar/", '_system', 'location=yes');
        return false;
    });
    
    /* button  TWITTER  */
    $(document).on("click", "#btnTwitter", function(evt)
    {
        /*global activate_page */
        window.open("https://twitter.com/ekartarg", '_system', 'location=yes');
        return false;
    });
    
    /* button  E-MAIL  */
    $(document).on("click", "#btnEmail", function(evt)
    {
        /*global activate_page */
        
        cordova.plugins.email.open({
            to:      'info@e-kart.com.ar',
            subject: 'Contacto desde la app E-Kart WebRadio'
        });

        return false;
    });
    
}


app.isPlaying = false;

app.changeToPlayPause = function(){
    if(app.isPlaying){
        app.my_media.stop();
        app.isPlaying = false;
    }else{
        app.my_media.play();
        app.isPlaying = true;
    }
}

app.crearMenu = function(idSenial){
    var senialActual = app.seniales[idSenial];
    var idMenuInterno = "#menuInterno .dropdown";
    // Limpiar el menu
    $(idMenuInterno).html("");

    var template = $('#templateMenuInterno').html();
    
    Mustache.parse(template);   // optional, speeds up future uses
    console.log(senialActual);

    var rendered = Mustache.render(template, {
        menu: senialActual['menu']
    });

    $(rendered).appendTo(idMenuInterno);

}
app.mostrarPublicidades = function(idSenial){
    // console.log(app.publicidades[idSenial]);
    var publicidadesSenial = app.publicidades[idSenial];
    publicidadesSenial.getPublicidades(function(data){
        publicidadesSenial.publicidades = [];

        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var publicidad = new PublicidadClass(item.fecha_desde,item.fecha_hasta,item.imagen,item.link); // Convierto cada objecto en una clase publicidad
            publicidadesSenial.publicidades[i] = publicidad; // La agrego al listado de las publicidades de esta señal

            console.log(publicidad.estaHabilitada());
        }
        publicidadesSenial.showPublicidades();
    })
}

app.eliminarPublicidades = function(){
    $("#publicidades-reproductor").html();
}
document.addEventListener("app.Ready", register_event_handlers, false);
    
})();

