/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested initialization place for your code.
// It is completely optional and not required.
// It implements a Cordova "hide splashscreen" function, that may be useful.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false, app:false, dev:false */
/*global myEventHandler:false, cordova:false, device:false */



window.app = window.app || {} ;         // there should only be one of these...

// app.servidor = "http://192.168.0.15/ekart-webradio/"; // Servidor que contiene los json
app.servidor = "http://e-kart.com.ar/"; // Servidor que contiene los json

// app.urlJson = "http://e-kart.com.ar/transmisionesVivo.txt?_=" + new Date().getTime();
app.urlJson = app.servidor + "WebRadio/transmisionesVivo.txt?_=" + new Date().getTime();

app.cantidadSeniales = 0;

app.seniales = [];
app.isPlaying = false; // Variable para comprobar si la radio está reproduciendo o no y mostrar visualmentel el boton play/pause en su caso

app.publicidades = [];

app.animacionPublicidades = null; // Conoce si se quedó animando las publicidades o no
// Set to "true" if you want the console.log messages to appear.

app.LOG = app.LOG || false ;

app.consoleLog = function() {           // only emits console.log messages if app.LOG != false
    if( app.LOG ) {
        var args = Array.prototype.slice.call(arguments, 0) ;
        console.log.apply(console, args) ;
    }
} ;



// App init point (runs on custom app.Ready event from init-dev.js).
// Runs after underlying device native code and webview/browser is ready.
// Where you should "kick off" your application by initializing app events, etc.

// NOTE: Customize this function to initialize your application, as needed.

app.initEvents = function() {
    "use strict" ;
    var fName = "app.initEvents():" ;
    app.consoleLog(fName, "entry") ;

    // NOTE: initialize your third-party libraries and event handlers

    // initThirdPartyLibraryNumberOne() ;
    // initThirdPartyLibraryNumberTwo() ;
    // initThirdPartyLibraryNumberEtc() ;

    // NOTE: initialize your application code

    // initMyAppCodeNumberTwo() ;
    // initMyAppCodeNumberEtc() ;
    
//    intel.xdk.device.setRotateOrientation("portrait");
    
    var $loading = $('#preloader').hide();
    $(document)
      .ajaxStart(function () {
        $loading.show();
      })
      .ajaxStop(function () {
        $loading.hide();
      });
    // NOTE: initialize your app event handlers, see app.js for a simple event handler example

    // TODO: configure following to work with both touch and click events (mouse + touch)
    // see http://msopentech.com/blog/2013/09/16/add-pinch-pointer-events-apache-cordova-phonegap-app/

//...overly simple example...
//    var el, evt ;
//
//    if( navigator.msPointerEnabled || !('ontouchend' in window))    // if on Win 8 machine or no touch
//        evt = "click" ;                                             // let touch become a click event
//    else                                                            // else, assume touch events available
//        evt = "touchend" ;                                          // not optimum, but works
//
//    el = document.querySelector(".itemRadio") ;
//    el.addEventListener(evt, app.goToRadio, false) ;

    // NOTE: ...you can put other miscellaneous init stuff in this function...
    // NOTE: ...and add whatever else you want to do now that the app has started...
    // NOTE: ...or create your own init handlers outside of this file that trigger off the "app.Ready" event...
    
    app.initDebug() ;           // just for debug, not required; keep it if you want it or get rid of it
    app.hideSplashScreen() ;    // after init is good time to remove splash screen; using a splash screen is optional

    // app initialization is done
    // app event handlers are ready
    // exit to idle state and wait for app events...
    initLoadRadios() ;
    app.consoleLog(fName, "exit") ;
} ;
document.addEventListener("app.Ready", app.initEvents, false) ;



// Just a bunch of useful debug console.log() messages.
// Runs after underlying device native code and webview/browser is ready.
// The following is just for debug, not required; keep it if you want or get rid of it.

app.initDebug = function() {
    "use strict" ;
    var fName = "app.initDebug():" ;
    app.consoleLog(fName, "entry") ;

    if( window.device && device.cordova ) {                     // old Cordova 2.x version detection
        app.consoleLog("device.version: " + device.cordova) ;   // print the cordova version string...
        app.consoleLog("device.model: " + device.model) ;
        app.consoleLog("device.platform: " + device.platform) ;
        app.consoleLog("device.version: " + device.version) ;
    }

    if( window.cordova && cordova.version ) {                   // only works in Cordova 3.x
        app.consoleLog("cordova.version: " + cordova.version) ; // print new Cordova 3.x version string...

        if( cordova.require ) {                                 // print included cordova plugins
            app.consoleLog(JSON.stringify(cordova.require('cordova/plugin_list').metadata, null, 1)) ;
        }
    }

    app.consoleLog(fName, "exit") ;
} ;



// Using a splash screen is optional. This function will not fail if none is present.
// This is also a simple study in the art of multi-platform device API detection.

app.hideSplashScreen = function() {
    "use strict" ;
    var fName = "app.hideSplashScreen():" ;
    app.consoleLog(fName, "entry") ;

    // see https://github.com/01org/appframework/blob/master/documentation/detail/%24.ui.launch.md
    // Do the following if you disabled App Framework autolaunch (in index.html, for example)
    // $.ui.launch() ;

    if( navigator.splashscreen && navigator.splashscreen.hide ) {   // Cordova API detected
        navigator.splashscreen.hide() ;
    }
    if( window.intel && intel.xdk && intel.xdk.device ) {           // Intel XDK device API detected, but...
        if( intel.xdk.device.hideSplashScreen )                     // ...hideSplashScreen() is inside the base plugin
            intel.xdk.device.hideSplashScreen() ;
    }

    app.consoleLog(fName, "exit") ;
} ;


function initLoadRadios(){
    $(".msgCargando").show(); // Mostrar mensaje de cargando...
    $.ajax({
        type: "GET",
        url: app.urlJson,
        timeout: 30000,
        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
        dataType: 'json',
        crossDomain: true
    }).done(function(data){
        $(".msgCargando").hide();
        var json = jQuery.parseJSON(JSON.stringify(data));
        var radios = json["radios"]
        app.seniales = radios;
        app.cantidadSeniales = app.seniales.length;
        for(var i = 0; i < app.cantidadSeniales; i++)
        {
            app.crearItemRadio(radios[i], i);
        }   
    }).fail(function(data, textStatus, xhr){
        $(".msgCargando").hide();
        //This shows status code eg. 403
        console.log("error ", data.status);
        //This shows status message eg. Forbidden
        console.log("STATUS: "+xhr);
        window.alert("No hemos podido cargar las radios. Comprueba tu conexión a internet, cierra y vuelve a abrir la aplicación.")
    });
    
}



app.crearItemRadio = function(obj,index) {
    var template = $('#templateItemRadios').html();
    
    var senialIndex = index+1;
    
    // Crear objeto publicidades con la url tomada de aca!
    var publicidadesSenialObj = new publicidadesSenial(index+1, obj.publicidades);
    app.publicidades.push(publicidadesSenialObj);

    Mustache.parse(template);   // optional, speeds up future uses
    index = index == 0? 0: senialIndex;
    esOffline = obj.estado == 0? true : false;

    var rendered = Mustache.render(template, {
        titulo: obj.titulo, 
        subtitulo: obj.subtitulo, 
        url: obj.url, 
        index: index, 
        estado: esOffline
    });
    $(rendered).appendTo('#listadoRadios');

}



function publicidadesSenial(senial, urlPublicidades){
    this.senial             = senial;
    this.url                = urlPublicidades;
    this.publicidades       = [];
    this.idArea             = "#publicidades-reproductor";
    this.getPublicidades    = function (callback){
        var self = this;
        $.ajax({
            type: "GET",
            url: app.servidor + "" + urlPublicidades + "?_=" + new Date().getTime(),
            timeout: 30000,
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            dataType: 'json',
            crossDomain: true
        }).done(function(data){
            var json = jQuery.parseJSON(JSON.stringify(data));
            var publicidades = json["publicidades"];
            self.publicidades = publicidades;
            callback(self.publicidades);

        }).fail(function(data, textStatus, xhr){
            //This shows status code eg. 403
            console.log("error publicidades", data.status);
            //This shows status message eg. Forbidden
            console.log("STATUS: "+xhr);
            // window.alert("No hemos podido cargar las radios. Comprueba tu conexión a internet, cierra y vuelve a abrir la aplicación.")
        });
    };
    this.showPublicidades   = function(){
        var self = this;
        $(self.idArea).html(""); // Limpiar otras publicidades
        console.log(self.publicidades);
        if(self.publicidades.length > 0){
            var template = $('#publicidadesTemplate').html();
    
            Mustache.parse(template);   // optional, speeds up future uses

            var rendered = Mustache.render(template, {
                publicidades: self.publicidades
            });
            $(rendered).appendTo(self.idArea);
            self.initPublicidades();
        }

    };
    this.initPublicidades   = function() {
        /* Slider Publicidades */
        var self = this;
        var activo = 1;
        var cantPublicidades = $(self.idArea + ' ul li').length;

        if (app.animacionPublicidades != null){
            clearInterval(app.animacionPublicidades);
            app.animacionPublicidades = null;
        }

        if (cantPublicidades > 1){
            $(self.idArea + ' ul li').hide();
            $(self.idArea + ' ul li:nth-child('+activo+')').show();

            app.animacionPublicidades = setInterval(function () {
                activo = activo + 1;
                activo = activo > cantPublicidades ? 1 : activo;

                $(self.idArea + ' ul li').hide();
                $(self.idArea + ' ul li:nth-child('+activo+')').show();
            }, 20000);
        }
    };
    
}

function PublicidadClass(fecha_desde, fecha_hasta, imagen, link){
    this.fecha_desde        = fecha_desde;
    this.fecha_hasta        = fecha_hasta;
    this.imagen             = imagen;
    this.link               = link;
    this.estaHabilitada     = function() {
        // Obtengo las fechas, las divido por arrays para obtener año, mes y día de cada fecha
        // Creo objetos fecha para luego obtener el tiempo (getTime)
        // Comparo si el día de hoy está entre las fechas desde y hasta
        // return true si esta entre las fechas desde y hasta
        // return false si esta fuera de las fechas desde y hasta

        var self = this;

        var date_time_hoy = new Date().getTime();

        var partes_fecha_desde = self.fecha_desde.split('/');

        var date_desde = new Date(partes_fecha_desde[2],partes_fecha_desde[1],partes_fecha_desde[0]).getTime(); 

        var partes_fecha_hasta = self.fecha_hasta.split('/');

        var date_hasta = new Date(partes_fecha_hasta[2],partes_fecha_hasta[1],partes_fecha_hasta[0]).getTime(); 

        return (date_desde <= date_time_hoy && date_time_hoy <= date_hasta);

    }
}
