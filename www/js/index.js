// Wait for device API libraries to load
    //
    document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    //
    function onDeviceReady() {
        navigator.splashscreen.show();
    }

var vid = $("audio");
//window.alert("hola");

function handleSourceError(e) { alert('Error loading: '+e.target.src); }
function handleMediaError(e) {
    var popupR;
    var popupC;
    switch (e.target.error.code) {
        case e.target.error.MEDIA_ERR_ABORTED:
            popupR = confirm('Se ha interrumpido la reproducción de la radio.\n¿Deseas volver a iniciar la transmisión?'); break;
        case e.target.error.MEDIA_ERR_NETWORK:
            popupR = confirm('Error con la conexión a internet.\n¿Deseas volver a iniciar la transmisión?'); break;
        case e.target.error.MEDIA_ERR_DECODE:
            popupC = confirm('La transmisión de la radio ha sido interrumpida por un problema de compatibilidad.\n¿Deseas informar y cerrar la aplicacion?'); break;
        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
            popupR = confirm('No se pudo cargar la transmisión porque el servidor o la red fallaron o porque el formato no es compatible.\n¿Deseas volver a iniciar la transmisión?'); break;
        default:
            popupC = confirm('Se ha producido un error desconocido de la transmisión.\n¿Deseas informar y cerrar la aplicacion?');
    }
    
    if(popupR){
        window.location.reload(true);
    }
    
    if(popupC) {
        window.close();
    }
    
}

var toArray = Array.prototype.slice;
toArray.apply(document.getElementsByTagName('audio')).forEach(function(audio){
    audio.addEventListener('error', handleMediaError);
    toArray.apply(audio.getElementsByTagName('source')).forEach(function(source){
        source.addEventListener('error', handleSourceError);
    });
});

app.my_media;

app.goToRadio = function(url){
    app.hideLoading(); //Ocultar el mensaje de "Cargando..."
//    app.my_media = new Media(url, onSucess, onError, onStatus);
    app.my_media = new PlayStream(url, onStatus, onError);
    app.changeToPlayPause();
}

function onSucess(media){
    console.log("El audio llegó a su final o está en pausa");
}

function onError(e){
    console.log("Error:" + e);
}

function onStatus(status){
    var icono = $(".btnPlayPause");
    console.log("cargando...?");
    if ( status == PlayStream.MEDIA_STARTING ){
        console.log("Está cargando el streaming");
        app.showLoading();
    }
    else if ( status == PlayStream.MEDIA_RUNNING ){
        $(icono).removeClass('play');
        $(icono).removeClass('pause');    
        console.log("Está reproduciendo el audio stream.");
        app.hideLoading();
        $(icono).addClass('pause');
    }
    else if( status == PlayStream.MEDIA_STOPPED ){
        $(icono).removeClass('play');
        $(icono).removeClass('pause');    
        console.log("Se puso en pausa o stop el audio.");
        app.hideLoading();
        $(icono).addClass('play');
    }
    else {
        $(icono).removeClass('play');
        $(icono).removeClass('pause');    
        console.log("Status:" + status);
        $(icono).addClass('play');
    }
}

app.showLoading = function(){
    $("#msgLoading").show();
}

app.hideLoading = function(){
    $("#msgLoading").hide();
}
